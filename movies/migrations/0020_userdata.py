# Generated by Django 2.2.4 on 2020-08-31 15:26

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0019_remove_movies_user_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('user_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('username', models.CharField(blank=True, max_length=255)),
                ('firebase', models.CharField(max_length=255)),
                ('dob', models.DateField(blank=True)),
                ('gmail', models.EmailField(max_length=254)),
                ('phone_number', models.IntegerField(blank=True)),
            ],
        ),
    ]
