# Generated by Django 2.2.4 on 2020-08-25 05:12

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0013_auto_20200825_0505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='moviewishlist',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2020, 8, 25, 5, 12, 31, 174292), verbose_name='date'),
        ),
        migrations.AlterField(
            model_name='moviewishlist',
            name='title',
            field=models.CharField(max_length=100),
        ),
    ]
