# Generated by Django 2.2.4 on 2020-09-06 14:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0021_auto_20200906_1350'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userdata',
            old_name='gmail',
            new_name='email_id',
        ),
    ]
