# Generated by Django 2.2.4 on 2020-08-23 20:13

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0008_auto_20200823_1958'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userwishlist',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2020, 8, 23, 20, 13, 46, 202851), verbose_name='date'),
        ),
        migrations.AlterField(
            model_name='userwishlist',
            name='username',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user_name'),
        ),
    ]
