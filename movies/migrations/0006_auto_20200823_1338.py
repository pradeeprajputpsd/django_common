# Generated by Django 2.2.4 on 2020-08-23 13:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('movies', '0005_auto_20200823_1247'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user_data',
            old_name='user_id',
            new_name='id',
        ),
        migrations.AddField(
            model_name='user_data',
            name='user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
