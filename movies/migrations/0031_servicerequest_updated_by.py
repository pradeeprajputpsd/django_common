# Generated by Django 2.2.4 on 2020-09-13 22:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0030_auto_20200913_1912'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicerequest',
            name='updated_by',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
