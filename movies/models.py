from django.db import models
import uuid
from django.contrib.auth.models import User
# Create your models here.
from datetime import datetime
from django.contrib.auth.models import AbstractUser
from multiselectfield import MultiSelectField


class Movies(models.Model):
    title = models.CharField(max_length=100)
    year = models.IntegerField()
    rating = models.FloatField()
    image_url = models.URLField(max_length=1000)
    redirect_url = models.URLField()
    
    
class MovieWishlist(models.Model):
    username = models.CharField(max_length=100)
    movie = models.ForeignKey(Movies, on_delete=models.CASCADE, null=True)


class UserData(models.Model):
    user_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(blank=True, max_length=255,null= True)
    firebase_id = models.CharField(max_length=255,blank = True)
    dob = models.DateField(blank = True, null= True)
    email_id = models.EmailField(max_length=254,blank = True,null= True)
    phone_number = models.IntegerField(blank=True,null= True)
    created_at = models.DateTimeField(default=datetime.now, blank=True)


class ServiceRequest(models.Model):
    STATUS_OPTION = (
                    ('Pending', 'Pending'),
                    ('In Progress', 'In Progress'),
                    ('Completed', 'Completed'),
                    )
    username = models.CharField( max_length=255)
    request_type = models.CharField( max_length=255)
    request_desc = models.TextField()
    city = models.CharField(max_length=254,blank = True,null= True)
    state = models.CharField( max_length=255)
    pin_code = models.IntegerField()
    phone_code = models.CharField(blank=True, max_length=3,null= True)
    alternate_phone_number = models.IntegerField(blank=True,null= True)
    status = models.CharField(max_length=255,blank = True,null= True, choices=STATUS_OPTION, default='Pending', editable=True)
    remarks = models.TextField(blank = True,null= True)
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    updated_by = models.CharField( max_length=255,blank = True,null= True)
    









