from . import views

from django.urls import include, path
from rest_framework import routers
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView # new
from django.views.generic import RedirectView
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy
from rest_framework.routers import DefaultRouter
from movies.views import signUp
from movies.form import RequestForm


app_name = "movies"

router = DefaultRouter()
# router.register('login_api', LoginAPI, basename = 'login_api')

urlpatterns = [
#     path('admin/', admin.site.urls),
    
    path('rest_api', include(router.urls)), 
    path('', TemplateView.as_view(template_name='home.html'), name='home'), # new
    #path('login', views.login),
#     path('logout', views.logout_view), 
    path("register/", views.signup, name="register"),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('signup', signUp.as_view(), name = 'signup'),
    path('movie_list',views.movieList,name = 'movie_list'),
    path('wishlist',views.wishList,name = 'wishlist'),
    path('user',csrf_exempt(views.userLogin),name = 'user'),
    
    path('service_request/', views.serviceRequest, name='service_request'), # new
    path('view_service_request/', views.ViewSevice, name='view_service_request'), # new
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form.html',success_url = reverse_lazy('password_reset_done')), name='password_reset'),
    path('password_reset/done', auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.html'), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html'), name='password_reset_complete'),
    
    path('movie_search',views.movieSearch,name = 'movie_search')
]