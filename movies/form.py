from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User  #importing user model
from django.core.validators import RegexValidator

class NewUserForm(UserCreationForm):   #modifying defualt usercreationform and adding email field extra
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
        help_texts = {
            'username': None,
        }

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user    


from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    phone_number = forms.CharField(max_length=13, required=True)
    
    class Meta:
        model = User
        fields = ( 'username','first_name','last_name', 'phone_number', 'email', 'password1', 'password2', )
        widgets = {'username': forms.HiddenInput()}
   
        
class RequestForm(forms.Form):
    REQUEST_CHOICES = (
                    ('Plumbing', 'Plumbing'),
                    ('Electrical', 'Electrical'),
                    ('Painting', 'Painting'),
                    ('Deep Cleaning', 'Deep Cleaning'),
                    )
    request_type = forms.MultipleChoiceField(
        choices = REQUEST_CHOICES, # this is optional
        widget  = forms.CheckboxSelectMultiple,
        )
    request_desc = forms.CharField(widget=forms.Textarea,required=True)
    city = forms.CharField(max_length=254,required=True)
    state = forms.CharField( max_length=255,required=True)
    pin_code = forms.IntegerField(required=True)
    phone_code = forms.CharField( max_length=3,required=False)
    alternate_phone_number = forms.IntegerField(required=False)
    
        