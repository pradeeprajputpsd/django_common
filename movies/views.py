from django.shortcuts import render, redirect
from .serializers import UserSerializer,User_register,MoviesSerializer,WishlistSerializer,MoviesSerializer1
from .models import Movies,MovieWishlist, UserData, ServiceRequest
from .form import NewUserForm, SignUpForm , RequestForm
    
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from bs4 import BeautifulSoup
import requests
import json
from rest_framework import status, serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework.permissions import AllowAny
from rest_framework.authtoken.models import Token
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.pagination import PageNumberPagination

from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth import login as auth_login, logout, authenticate
from django.contrib import messages


@api_view(['POST','GET'])
@permission_classes((AllowAny,))
def sample_api(request):
    if request.method == 'GET':
        data = {'sample_data get method': 123}
        return Response(data, status=HTTP_200_OK)

    elif request.method == 'POST':
        data = {'sample_data post method': 123}
        return Response(data, status=HTTP_200_OK)


class signUp(APIView):
    permission_classes = [AllowAny]
    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            content = {
            'status': 'Thanks for registering'}
            return Response(content, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username") 
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_400_BAD_REQUEST)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)  
    


@api_view(["POST","GET"])
# @permission_classes((AllowAny,))
def movieList(request):
    user = request.user
    movie_id = request.query_params.get('id')
    movie_title = request.query_params.get('title')
    if request.method == 'GET':
        paginator = PageNumberPagination()
        paginator.page_size = 10
        context = {"username": user}
        
        if movie_id or movie_title:
            try:
                if movie_id:
                    movies_obj = Movies.objects.get(id=movie_id)
                else:
                    movies_obj = Movies.objects.get(title=movie_title)
                serializer = MoviesSerializer1(movies_obj,context=context, many=False)
                return Response(serializer.data)
            except Movies.DoesNotExist:
                movies_obj = None
        else:
            movies_obj = Movies.objects.all()
            result_page = paginator.paginate_queryset(movies_obj, request)
            serializer = MoviesSerializer1(result_page,context=context, many=True)
        if movies_obj == None:
           return Response({'error': 'Please provide valis movie id or title'},
                        status=HTTP_400_BAD_REQUEST) 
        return paginator.get_paginated_response(serializer.data)
    
    if request.method == 'POST':
        """url= 'https://www.imdb.com/chart/top/'
           url = 'https://www.imdb.com/india/top-rated-indian-movies/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=461131e5-5af0-4e50-bee2-223fad1e00ca&pf_rd_r=6SSJ32D3EGD0SPY3SS7E&pf_rd_s=center-1&pf_rd_t=60601&pf_rd_i=india.toprated&ref_=fea_india_ss_toprated_india_tr_india250_hd'
        """
        scrap_url = request.data.get("scrap_url") 
        if scrap_url is None:
            return Response({'error': 'Please provide scrap url'},
                            status=HTTP_400_BAD_REQUEST)
        response_data = requests.get(scrap_url)
        soup = BeautifulSoup(response_data.text, 'html.parser')
        mydivs = soup.findAll("tbody", {"class": "lister-list"})[0].findAll("tr")
        for each in mydivs:
            image_url = each.findAll("td",{"class":"posterColumn"})[0].find('a').img['src']
            title = each.findAll("td",{"class":"titleColumn"})[0].find('a').text
            redirect_tag = each.findAll("td",{"class":"titleColumn"})[0].find('a')['href']
            redirect_url = 'https://www.imdb.com'+redirect_tag
            year = (each.findAll("td",{"class":"titleColumn"})[0].find('span').text)[1:5]
            rating = each.findAll("td",{"class":"ratingColumn imdbRating"})[0].find('strong').text
            
            obj, created = Movies.objects.update_or_create(title=title,
                                                           year=int(year),
                                                           rating=rating,
                                                           image_url=image_url,
                                                           redirect_url=redirect_url)

        return Response({'result': "successfully saved"},
                        status=HTTP_200_OK) 
    
  
 
@api_view(["GET","POST","DELETE"])
# @permission_classes((AllowAny,))
def wishList(request):
    user = request.user
    
    if request.method == 'GET':
        wishlist_obj = MovieWishlist.objects.filter(username=user)
        if wishlist_obj:
            serializer = WishlistSerializer(wishlist_obj, many=True)
            return Response(serializer.data) 
        else:
            return Response({'result': "no records found"},
                        status=HTTP_200_OK) 
    
    if request.method == 'POST':
        movie_id = request.data.get("movie_id")
        obj, created = MovieWishlist.objects.update_or_create(movie_id=movie_id,
                                                              username = user
                                                              )
        return Response({'result': 'wishlist saved'},
                        status=HTTP_200_OK) 
    
    if request.method == 'DELETE':
        wishlist_id = request.data.get("wishlist_id")
        try:
            wishlist_row = MovieWishlist.objects.get(id=wishlist_id)
        except MovieWishlist.DoesNotExist:
            wishlist_row = None
        if wishlist_row == None:
            return Response({'error': 'Please provide valid wishlist id'},
                        status=HTTP_400_BAD_REQUEST)
        wishlist_row.delete()
        return Response({'result': 'successfully deleted'},
                        status=HTTP_200_OK) 
  
 
@api_view(["GET"])
# @permission_classes((AllowAny,))
def movieSearch(request):
    user = request.user
    movie_title = request.query_params.get('title')
    if request.method == 'GET':
        try:
            movie_row = Movies.objects.filter(title__icontains=movie_title)
        except Movies.DoesNotExist:
            movie_row = None
        if movie_row:
            context = {"username": user}
            serializer = MoviesSerializer1(movie_row,context=context, many=True)
            return Response(serializer.data)
        else:
            return Response({'result': "no record found"},
                        status=HTTP_200_OK) 
    
 
@api_view(["POST"])
@permission_classes((AllowAny,))
def userLogin(request):
    json_data=json.loads(request.body)
    firebase_id = json_data.get("firebase_id")
    username = json_data.get("username")
    email_id = json_data.get("email_id")
    
    if firebase_id is None or email_id is None:
        return Response({'error': 'Please provide correct firebase id and email id'},
                        status=HTTP_400_BAD_REQUEST)
    try:
        user_row = User.objects.get(username=username)
    except User.DoesNotExist:
        user_row = None
    
    if user_row is not None:
        token, _ = Token.objects.get_or_create(user=user_row)
        data = {'token': token.key,'username':username,'email_id':email_id}
        return JsonResponse({'result':'SUCCESS','data':data,'message': 'Logged in successfully'},
                    status=HTTP_200_OK)
    else:
        print("pradeep2")
        user_row, _ = User.objects.get_or_create(username=username)
        user_data= UserData(firebase_id=firebase_id,email_id=email_id,username=username).save()
        token, _ = Token.objects.get_or_create(user=user_row)
        data = {'token': token.key,'username':username,'email_id':email_id}
        return JsonResponse({'result':'SUCCESS','data':data,'message': 'Logged in successfully'},
                    status=HTTP_200_OK)
    
#     user = authenticate(username=username, password=password)
#     if not user:
#         return Response({'error': 'Invalid Credentials'},
#                         status=HTTP_400_BAD_REQUEST)
#     token, _ = Token.objects.get_or_create(user=user)
#     return Response({'token': token.key},
#                     status=HTTP_200_OK)   
 
 
 


def logout_view(request):
    logout(request)
    return redirect('/')
 

def register(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        print("pradeep")
        if form.is_valid():
            print("pradeep1")
            user = form.save()
            username = form.cleaned_data.get('username')
            print(username)
            messages.success(request,f"New Account created: {username}")
            auth_login(request, user)
            messages.info(request, f"You are now logged in as: {username}")
            return redirect('/')
        else:
            for msg in form.error_messages:
                print(form.error_messages[msg])
    form = NewUserForm
    return render(request, "register.html", context={"form":form})
 

def signup(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        post = request.POST.copy() # to make it mutable
        post['username'] = email
        request.POST = post
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            auth_login(request, user)
            user_data= UserData(email_id=email,username=email,phone_number=form.cleaned_data.get('phone_number')).save()
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})
 

def serviceRequest(request):
    if request.method == 'POST':
        username = request.user.username
        form = RequestForm(request.POST)
        if form.is_valid():
            request_type = form.cleaned_data.get('request_type')
            request_type_values = ','.join([str(elem) for elem in request_type]) 
            request_desc = form.cleaned_data.get('request_desc')
            city = form.cleaned_data.get('city')
            state = form.cleaned_data.get('state')
            pin_code = form.cleaned_data.get('pin_code')
            phone_code = form.cleaned_data.get('phone_code')
            alternate_phone_number = form.cleaned_data.get('alternate_phone_number')
            ServiceRequest(username = username,request_type=request_type_values, request_desc=request_desc,
                           city=city, state=state, pin_code=pin_code, phone_code=phone_code, alternate_phone_number=alternate_phone_number
                             ).save()
            #return redirect('/')
            return render(request, 'service_request_form.html', {'msg': True})
    else:
        form = RequestForm()
    return render(request, 'service_request_form.html', {'form': form})
    

def ViewSevice(request):
    id = request.GET.get('data')
    username = request.user.username
    if id:
        return render(request=request, template_name="view_service_request.html", context={"content_by_id": ServiceRequest.objects.get(id = id, username = username)})
    return render(request=request, template_name="view_service_request.html", context={"content": ServiceRequest.objects.filter(username = username)})











    
  