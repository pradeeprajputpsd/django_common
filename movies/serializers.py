from django.contrib.auth.models import User, Group
from rest_framework import serializers
from movies.models import Movies, MovieWishlist
    
    
class User_register(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','email','password')
        
 
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','password','email')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user  
    

class MoviesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movies
        fields = ('title','year','rating','image_url','redirect_url') 

 
 
class WishlistSerializer(serializers.ModelSerializer):       
    class Meta:
        model = MovieWishlist
        fields = '__all__'
        depth = 1

       
class MoviesSerializer1(serializers.ModelSerializer):
    wish_list_flag = serializers.SerializerMethodField('get_wishlist_flag',read_only=True)
    
    class Meta:
        model = Movies
        fields = ('id','title','year','rating','image_url','redirect_url','wish_list_flag')       
        
    def get_wishlist_flag(self,obj):
        username = self.context.get('username')
        movie_id = obj.id
        data = MovieWishlist.objects.filter(username=username,movie_id=movie_id)
        if data:
            return True
        else:
            return False
          
        