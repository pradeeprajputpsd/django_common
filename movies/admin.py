from django.contrib import admin
from django.contrib.auth.models import User, Group
from .models import Movies,MovieWishlist,UserData, ServiceRequest
# Register your models here.



admin.site.register(ServiceRequest)
admin.site.register(MovieWishlist)
admin.site.register(Movies)
admin.site.register(UserData)


