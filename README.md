django-rest-framework movie rating app and Service Requests management web app

Screenshot URLs:

![Home_page](https://gitlab.com/pradeeprajputpsd/django_common/-/blob/master/images/home.png)

![Login_page](https://gitlab.com/pradeeprajputpsd/django_common/-/blob/master/images/login.png)

![Home_page_After_login](https://gitlab.com/pradeeprajputpsd/django_common/-/blob/master/images/home_after_login.png)

![Registration_page](https://gitlab.com/pradeeprajputpsd/django_common/-/blob/master/images/registration.png)

![New_Request_add_page](https://gitlab.com/pradeeprajputpsd/django_common/-/blob/master/images/add_service_requests.png)

![Request_list_by_user_page](https://gitlab.com/pradeeprajputpsd/django_common/-/blob/master/images/request_list.png)

![Detailed_request_view_page](https://gitlab.com/pradeeprajputpsd/django_common/-/blob/master/images/request_detail_view.png)


NOTE:

    For Password Reset email sending, Please do configure in settings.py file
    EMAIL_HOST_USER = 'xxxxxxxxxx@gmail.com'
    EMAIL_HOST_PASSWORD = 'xxxxxxxxxx'
    DEFAULT_FROM_EMAIL = 'xxxxxxxxxx@gmail.com'
    SERVER_EMAIL = 'xxxxxxxxxx@gmail.com'

    And Allow Less secure app permission for your email using below url:
    https://myaccount.google.com/lesssecureapps



Initial setup:

    git clone
    cd django_common

create virtual environment:

    virtualenv venv
    source venv/bin/activate


install requirements and run:

    pip3 install -r requirements.txt

    python3 manage.py makemigrations
    python3 manage.py migrate
    python3 manage.py runserver


postman api collection public url:
    
    https://documenter.getpostman.com/view/9961078/TVCZZr2v

SetUp by docker file:
    
    git clone
    cd  django_common
    docker-compose up

